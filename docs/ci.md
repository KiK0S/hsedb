# Как сдавать задачи

Запустите скрипт посылки из директории с задачей. python3 ../submit.py.

На главной https://hsedb.manytask.org/ есть ссылка на список ваших посылок - Submits. Там вы можете следить за прогрессом последней посылки.

Проверьте, что оценка появилась в [Таблице с результатами](https://docs.google.com/spreadsheets/d/1tJ0FdaukKi25aaLPdNRM9KTECYX8Gx6exxYi1kTLjUY/edit#gid=0)
