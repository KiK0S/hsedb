//===----------------------------------------------------------------------===//
//
//                         BusTub
//
// parallel_buffer_pool_manager_test.cpp
//
// Identification: test/buffer/buffer_pool_manager_test.cpp
//
// Copyright (c) 2015-2021, Carnegie Mellon University Database Group
//
//===----------------------------------------------------------------------===//

#include "buffer/parallel_buffer_pool_manager.h"
#include <cstdio>
#include <random>
#include <memory>
#include <array>
#include "buffer/buffer_pool_manager.h"
#include "gtest/gtest.h"

namespace bustub {

std::random_device r;
std::default_random_engine rng(r());
std::uniform_int_distribution<char> uniform_dist(0);

using Data = std::array<char, PAGE_SIZE>;

Data GetRandomBinaryData() {
  Data random_binary_data;
  for (char & symbol : random_binary_data) {
    symbol = uniform_dist(rng);
  }

  // Insert terminal characters both in the middle and at end
  random_binary_data[PAGE_SIZE / 2] = '\0';
  random_binary_data[PAGE_SIZE - 1] = '\0';

  return random_binary_data;
}


// Check whether pages containing terminal characters can be recovered
// NOLINTNEXTLINE
TEST(ParallelBufferPoolManagerTest, BinaryDataTest) {
  const std::string db_name = "test.db";
  const size_t buffer_pool_size = 10;
  const size_t num_instances = 5;

  auto disk_manager = std::make_unique<DiskManager>(db_name);
  auto bpm = std::make_unique<ParallelBufferPoolManager>(num_instances, buffer_pool_size, disk_manager.get());
  page_id_t page_id_temp;
  auto *page0 = bpm->NewPage(&page_id_temp);

  // Scenario: The buffer pool is empty. We should be able to create a new page.
  ASSERT_NE(nullptr, page0);
  EXPECT_EQ(0, page_id_temp);

  // Generate random binary data
  auto random_binary_data = GetRandomBinaryData();

  // Scenario: Once we have a page, we should be able to read and write content.
  std::memcpy(page0->GetData(), random_binary_data.data(), PAGE_SIZE);
  EXPECT_EQ(0, std::memcmp(page0->GetData(), random_binary_data.data(), PAGE_SIZE));

  // Scenario: We should be able to create new pages until we fill up the buffer pool.
  for (size_t i = 1; i < buffer_pool_size * num_instances; ++i) {
    EXPECT_NE(nullptr, bpm->NewPage(&page_id_temp));
  }

  // Scenario: Once the buffer pool is full, we should not be able to create any new pages.
  for (size_t i = buffer_pool_size; i < buffer_pool_size * num_instances * 2; ++i) {
    EXPECT_EQ(nullptr, bpm->NewPage(&page_id_temp));
  }

  // Scenario: After unpinning pages {0, 1, 2, 3, 4} we should be able to create 5 new pages
  for (int i = 0; i < 5; ++i) {
    EXPECT_EQ(true, bpm->UnpinPage(i, true));
    bpm->FlushPage(i);
  }
  for (int i = 0; i < 5; ++i) {
    EXPECT_NE(nullptr, bpm->NewPage(&page_id_temp));
    bpm->UnpinPage(page_id_temp, false);
  }

  // Scenario: We should be able to fetch the data we wrote a while ago.
  page0 = bpm->FetchPage(0);
  EXPECT_EQ(0, memcmp(page0->GetData(), random_binary_data.data(), PAGE_SIZE));
  EXPECT_EQ(true, bpm->UnpinPage(0, true));

  // Shutdown the disk manager and remove the temporary file we created.
  disk_manager->ShutDown();
  remove("test.db");
}

// NOLINTNEXTLINE
TEST(ParallelBufferPoolManagerTest, SampleTest) {
  const std::string db_name = "test.db";
  const size_t buffer_pool_size = 10;
  const size_t num_instances = 5;

  auto disk_manager = std::make_unique<DiskManager>(db_name);
  auto bpm = std::make_unique<ParallelBufferPoolManager>(num_instances, buffer_pool_size, disk_manager.get());

  page_id_t page_id_temp;
  auto *page0 = bpm->NewPage(&page_id_temp);

  // Scenario: The buffer pool is empty. We should be able to create a new page.
  ASSERT_NE(nullptr, page0);
  EXPECT_EQ(0, page_id_temp);

  // Scenario: Once we have a page, we should be able to read and write content.
  snprintf(page0->GetData(), PAGE_SIZE, "Hello");
  EXPECT_EQ(0, strcmp(page0->GetData(), "Hello"));

  // Scenario: We should be able to create new pages until we fill up the buffer pool.
  for (size_t i = 1; i < buffer_pool_size * num_instances; ++i) {
    EXPECT_NE(nullptr, bpm->NewPage(&page_id_temp));
  }

  // Scenario: Once the buffer pool is full, we should not be able to create any new pages.
  for (size_t i = buffer_pool_size; i < buffer_pool_size * num_instances * 2; ++i) {
    EXPECT_EQ(nullptr, bpm->NewPage(&page_id_temp));
  }

  // Write world out to page 4
  auto page4 = bpm->FetchPage(4);
  snprintf(page4->GetData(), PAGE_SIZE, "World");
  EXPECT_EQ(0, strcmp(page4->GetData(), "World"));
  bpm->UnpinPage(4, true);

  // Scenario: After unpinning pages {0, 1, 2, 3, 4} and pinning pages {0, 1, 2, 3},
  // there would still be one buffer page left for reading page 4.

  for (int i = 0; i < 5; ++i) {
    EXPECT_EQ(true, bpm->UnpinPage(i, true));
  }
  for (int i = 0; i < 4; ++i) {
    EXPECT_NE(nullptr, bpm->FetchPage(i));
  }

  // Scenario: We should be able to fetch the data we wrote a while ago.
  page4 = bpm->FetchPage(4);
  EXPECT_EQ(0, strcmp(page4->GetData(), "World"));

  // Scenario: If we unpin page 4 and then make a new page, all the buffer pages should
  // now be pinned. Fetching page 4 should fail.
  EXPECT_EQ(true, bpm->UnpinPage(4, true));
  EXPECT_NE(nullptr, bpm->NewPage(&page_id_temp));
  EXPECT_EQ(nullptr, bpm->FetchPage(4));

  // Shutdown the disk manager and remove the temporary file we created.
  disk_manager->ShutDown();
  remove("test.db");
}

// NOLINTNEXTLINE
TEST(ParallelBufferPoolManager, ParallelTest) {
  const std::string db_name = "test.db";
  const size_t buffer_pool_size = 100;
  const size_t num_instances = 11;
  const size_t num_threads = 4;

  auto disk_manager = std::make_unique<DiskManager>(db_name);
  auto bpm = std::make_unique<ParallelBufferPoolManager>(num_instances, buffer_pool_size, disk_manager.get());

  using Ethalon = std::unordered_map<page_id_t, Data>;

  Ethalon ethalon;
  std::mutex ethalon_mutex;

  /// It will just insert data to the byffer_pool
  auto insert_routine = [&]()
  {
    page_id_t page_id_temp;
    auto * page = bpm->NewPage(&page_id_temp);

    if (page == nullptr) {
      return;
    }

    // Generate random binary data
    auto random_binary_data = GetRandomBinaryData();

    // Scenario: Once we have a page, we should be able to read and write content.
    std::memcpy(page->GetData(), random_binary_data.data(), PAGE_SIZE);
    EXPECT_EQ(0, std::memcmp(page->GetData(), random_binary_data.data(), PAGE_SIZE));

    // Flush ??
    bpm->FlushPage(page_id_temp);

    // Add the data to the ethalon
    {
      std::lock_guard lock(ethalon_mutex);
      ethalon[page_id_temp] = random_binary_data;
    }

    // It is dirty, because we wrote data there
    bpm->UnpinPage(page_id_temp, /*is_dirty=*/true);
  };

  std::vector<std::thread> threads(num_threads);

  for (size_t i = 0; i < num_threads; ++i) {
    threads[i] = std::thread([&insert_routine](){
      for (size_t attempt = 0; attempt < 100; ++attempt) {
        insert_routine();
      }
    });
  }

  for (auto & thread: threads) {
    thread.join();
  }

  // Validate all the pages we wrote
  for (auto & [page_id, original_data] : ethalon)
  {
    auto * page = bpm->FetchPage(page_id);
    EXPECT_EQ(0, std::memcmp(page->GetData(), original_data.data(), PAGE_SIZE));
  }

}

// NOLINTNEXTLINE
TEST(ParallelBufferPoolManagerTest, DeleteTest) {
  const std::string db_name = "test.db";
  const size_t buffer_pool_size = 10;
  const size_t num_instances = 5;

  auto disk_manager = std::make_unique<DiskManager>(db_name);
  auto bpm = std::make_unique<ParallelBufferPoolManager>(num_instances, buffer_pool_size, disk_manager.get());

  for (int i = 0; i < 5; i++) {
    EXPECT_EQ(true, bpm->DeletePage(i));
  }

  page_id_t page_id_temp;
  auto page0 = bpm->NewPage(&page_id_temp);
  snprintf(page0->GetData(), PAGE_SIZE, "Hello");
  EXPECT_EQ(0, strcmp(page0->GetData(), "Hello"));
  EXPECT_EQ(false, bpm->DeletePage(0));
  bpm->UnpinPage(0, false);
  bpm->FlushAllPages();
  EXPECT_EQ(true, bpm->DeletePage(0));
}

// NOLINTNEXTLINE
TEST(ParallelBufferPoolManagerTest, FlushTest) {
  const std::string db_name = "test.db";
  const size_t buffer_pool_size = 10;
  const size_t num_instances = 5;

  auto disk_manager = std::make_unique<DiskManager>(db_name);
  auto bpm = std::make_unique<ParallelBufferPoolManager>(num_instances, buffer_pool_size, disk_manager.get());

  page_id_t page_id_temp;
  auto page0 = bpm->NewPage(&page_id_temp);
  snprintf(page0->GetData(), PAGE_SIZE, "Hello");
  EXPECT_EQ(0, strcmp(page0->GetData(), "Hello"));

  auto page1 = bpm->NewPage(&page_id_temp);
  snprintf(page1->GetData(), PAGE_SIZE, "World");
  EXPECT_EQ(0, strcmp(page1->GetData(), "World"));

  for (int i = 0; i < 2; i++) {
    EXPECT_EQ(true, bpm->UnpinPage(i, true));
  }

  bpm->FlushAllPages();
  page0 = bpm->FetchPage(0);
  EXPECT_EQ(0, strcmp(page0->GetData(), "Hello"));

  page1 = bpm->FetchPage(1);
  EXPECT_EQ(0, strcmp(page1->GetData(), "World"));
}



}  // namespace bustub
